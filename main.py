import os
from flask import Flask, request
from flask import jsonify
import time
import glob
import numpy as np
import pandas as pd

def standard_json_response(message='ok', code=200, data=None, to_json=True, success=True):
    message_json = {'message': message, 'data': data, 'code': code, 'success': success}
    print('standard_json_response')
    print(jsonify(message_json))
    return jsonify(message_json) if to_json else message_json

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():

    print("Hello World")  
    try:
        args = request.args
        user_id = args.get("user_id",  default="", type=str)
        try:
            data = {
                "user_id": str(user_id),
            }            
        except:
            data = {}

        return standard_json_response(data=data), 200
    except Exception as e:
        message = str(e)
        return standard_json_response(message=message, code=400, success=False), 400


if __name__ == "__main__":
    PORT = int(os.getenv("PORT")) if os.getenv("PORT") else 8080
    app.run(host="127.0.0.1", port=PORT, debug=True)
